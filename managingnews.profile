<?php

/**
 * Implementation of hook_profile_details().
 */
function managingnews_profile_details() {
  return array(
    'name' => 'Managing News',
    'description' => 'Managing News by Development Seed.'
  );
}

/**
 * Implementation of hook_profile_modules().
 */
function managingnews_profile_modules() {
    global $install_locale;
  // Drupal core
  $modules = array(
    'block',
    'book',
    'color',
    'dblog',
    'filter',
    'help',
    'menu',
    'node',
    'openid',
    'search',
    'system', 
    'taxonomy',
    'trigger',
    'update',
    'upload',
    'user',
    'context',
    'context_contrib',
    'context_ui',
    'features',
    'feedapi',
    'feedapi_data',
    'feedapi_mapper',
    'feedapi_tagger',
    'flot',
    'imageapi',
    'imageapi_gd',
    'imagecache',
    'libraries',
    'openidadmin',
    'parser_simplepie',
    'porterstemmer',
    'views',
    'views_ui',
    'views_rss',
    'views_modes',
    'schema',
    'seed',
    'strongarm',
    'geotaxonomy',
    'parser_csv',
    'mn_about',
    'mn_aggregator',
    'mn_search',
    'mn_shared',
    'mn_about',
    'mn_channels',
  );
  return $modules;
}

/**
 * Returns an array list of core mn modules.
 */
function _managingnews_core_modules() {
  return array(
    'ctools',
    'openlayers',
    'openlayers_ui',
    'openlayers_behaviors',
    'openlayers_views',
    'stored_views',
    'data',
    'data_ui',
    'data_search',
    'data_node',
  );
}

/**
 * Implementation of hook_profile_task_list().
 */
function managingnews_profile_task_list() {
  return array(
    'mn-configure' => st('Managing News configuration'),
  );
}

/**
 * Implementation of hook_profile_tasks().
 */
function managingnews_profile_tasks(&$task, $url) {
  global $install_locale;
  
  // Just in case some of the future tasks adds some output
  $output = '';

  if ($task == 'profile') {
    $modules = _managingnews_core_modules();
    $files = module_rebuild_cache();
    $operations = array();
    foreach ($modules as $module) {
      $operations[] = array('_install_module_batch', array($module, $files[$module]->info['name']));
    }
    $batch = array(
      'operations' => $operations,
      'finished' => '_managingnews_profile_batch_finished',
      'title' => st('Installing @drupal', array('@drupal' => drupal_install_profile_name())),
      'error_message' => st('The installation has encountered an error.'),
    );
    // Start a batch, switch to 'profile-install-batch' task. We need to
    // set the variable here, because batch_process() redirects.
    variable_set('install_task', 'profile-install-batch');
    batch_set($batch);
    batch_process($url, $url);
  }

  if ($task == 'mn-configure') {
    // Create all data tables.
    drupal_get_schema(NULL, TRUE);
    $ret = array();
    $tables = data_get_all_tables(TRUE);
    foreach ($tables as $table) {
      db_create_table($ret, $table->get('name'), $table->get('table_schema'));
    }

    // Create a default vocabulary for storing geo terms imported by the feed_term content type
    // and subsequently used by the feedapi_tagger parser. Used by the mn_aggregator feature.
    $data = array('name' => 'Locations', 'relations' => 1);
    taxonomy_save_vocabulary($data);
    variable_set('mn_aggregator_location_vocab', $data['vid']);
    variable_set('geotaxonomy_'. $data['vid'], 1);

    // Enable the right theme
    $state = array('values' => array('status' => array('jake' => 'jake'), 'theme_default' => 'jake', 'op' => t('Save configuration')));
    drupal_execute('system_themes_form', $state);

    // Kill the blocks.
    db_query("UPDATE blocks SET region = '' WHERE theme = 'jake'");

    // Create the admin role.
    db_query("INSERT INTO {role} (name) VALUES ('%s')", 'admin');

    // Create a non-user 1 admin user and rename user 1.
    $superuser = user_load(1);
    $account = new StdClass();
    $fields = array(
      'name' => $superuser->name,
      'mail' => $superuser->mail,
      'status' => 1,
      // Assume the newly created admin role takes rid 3.
      'roles' => array(3 => 'admin'),
    );
    $username = $superuser->name == 'superuser' ? 'uberuser' : 'superuser';
    user_save($superuser, array('name' => $username));
    $account = user_save($account, $fields);

    // Copy the password from user 1.
    $md5 = db_result(db_query("SELECT pass FROM {users} WHERE uid = 1"));
    db_query("UPDATE {users} SET pass = '%s' WHERE uid > 1", $md5);

    // Replace user 1 session with new user.
    // We can't always count on having a user session, so check first.
    global $user;
    if ($user) {
      $user = $account;
    }

    // Other variables worth setting.
    variable_set('site_footer', 'Powered by <a href="http://www.managingnews.com">Managing News</a>.');
    variable_set('site_frontpage', 'feeds');

    // Clear caches.
    drupal_flush_all_caches();

    $task = 'finished';
  }
  return $output;
}

/**
 * Finished callback for the modules install batch.
 *
 * Advance installer task to language import.
 */
function _managingnews_profile_batch_finished($success, $results) {
  variable_set('install_task', 'mn-configure');
}

/**
 * Implementation of hook_form_alter().
 */
function managingnews_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'install_configure') {
    $form['site_information']['site_name']['#default_value'] = 'Managing News';
    $form['site_information']['site_mail']['#default_value'] = 'admin@'. $_SERVER['HTTP_HOST'];
    $form['admin_account']['account']['name']['#default_value'] = 'admin';
    $form['admin_account']['account']['mail']['#default_value'] = 'admin@'. $_SERVER['HTTP_HOST'];
  }
}
