
MANAGING NEWS

RSS/Atom based news tracker. 

FEATURES

* Aggregate RSS/Atom news
* Show news as list or on a map
* Search news
* Bundle interesting news into channels
* Configurable location tagging
* Configurable maps

WORK IN PROGRESS

This install profile is currently being finalized. Not all dependencies are 
available publicly yet.

CREDITS

Designed and built by Development Seed.
Built with funding from Knight Foundation.